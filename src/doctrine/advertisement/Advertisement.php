<?php

declare(strict_types=1);

namespace Skadmin\Advertisement\Doctrine\Advertisement;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Advertisement
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Website;

    #[ORM\Column(type: Types::TEXT)]
    private string $affiliateScript;

    #[ORM\Column(type: Types::TEXT)]
    private string $affiliateContent;

    public function update(string $name, string $content, bool $isActive, string $website, ?string $imagePreview, string $affiliateScript, string $affiliateContent): void
    {
        $this->name     = $name;
        $this->content  = $content;
        $this->isActive = $isActive;
        $this->website  = $website;

        $this->affiliateScript  = $affiliateScript;
        $this->affiliateContent = $affiliateContent;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getAffiliateScript(): string
    {
        return $this->affiliateScript;
    }

    public function getAffiliateContent(): string
    {
        return $this->affiliateContent;
    }
}
