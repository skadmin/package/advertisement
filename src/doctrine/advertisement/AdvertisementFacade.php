<?php

declare(strict_types=1);

namespace Skadmin\Advertisement\Doctrine\Advertisement;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function count;
use function shuffle;

final class AdvertisementFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Advertisement::class;
    }

    public function create(string $name, string $content, bool $isActive, string $website, ?string $imagePreview, string $affiliateScript, string $affiliateContent): Advertisement
    {
        return $this->update(null, $name, $content, $isActive, $website, $imagePreview, $affiliateScript, $affiliateContent);
    }

    public function update(?int $id, string $name, string $content, bool $isActive, string $website, ?string $imagePreview, string $affiliateScript, string $affiliateContent): Advertisement
    {
        $advertisement = $this->get($id);
        $advertisement->update($name, $content, $isActive, $website, $imagePreview, $affiliateScript, $affiliateContent);

        if (! $advertisement->isLoaded()) {
            $advertisement->setSequence($this->getValidSequence());
        }

        $this->em->persist($advertisement);
        $this->em->flush();

        return $advertisement;
    }

    public function get(?int $id = null): Advertisement
    {
        if ($id === null) {
            return new Advertisement();
        }

        $advertisement = parent::get($id);

        if ($advertisement === null) {
            return new Advertisement();
        }

        return $advertisement;
    }

    public function getRandom(bool $onlyActive = false): ?Advertisement
    {
        $advertisements = $this->getAll($onlyActive);

        if (count($advertisements) === 0) {
            return null;
        }

        shuffle($advertisements);

        return $advertisements[0];
    }

    /**
     * @return Advertisement[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }
}
