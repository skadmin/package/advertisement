<?php

declare(strict_types=1);

namespace Skadmin\Advertisement\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Advertisement\BaseControl;
use Skadmin\Advertisement\Doctrine\Advertisement\Advertisement;
use Skadmin\Advertisement\Doctrine\Advertisement\AdvertisementFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory       $webLoader;
    private AdvertisementFacade $facade;
    private Advertisement       $advertisement;
    private ImageStorage        $imageStorage;

    public function __construct(?int $id, AdvertisementFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->advertisement = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->advertisement->isLoaded()) {
            return new SimpleTranslation('advertisement.edit.title - %s', $this->advertisement->getName());
        }

        return 'advertisement.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->advertisement->isLoaded()) {
            if ($identifier !== null && $this->advertisement->getImagePreview() !== null) {
                $this->imageStorage->delete($this->advertisement->getImagePreview());
            }

            $advertisement = $this->facade->update(
                $this->advertisement->getId(),
                $values->name,
                $values->content,
                $values->is_active,
                $values->website,
                $identifier,
                $values->affiliate_script,
                $values->affiliate_content
            );
            $this->onFlashmessage('form.advertisement.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $advertisement = $this->facade->create(
                $values->name,
                $values->content,
                $values->is_active,
                $values->website,
                $identifier,
                $values->affiliate_script,
                $values->affiliate_content
            );
            $this->onFlashmessage('form.advertisement.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $advertisement->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->advertisement = $this->advertisement;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.advertisement.edit.name')
            ->setRequired('form.advertisement.edit.name.req');
        $form->addText('website', 'form.advertisement.edit.website');
        $form->addCheckbox('is_active', 'form.advertisement.edit.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('image_preview', 'form.advertisement.edit.image-preview');

        // TEXT
        $form->addTextArea('content', 'form.advertisement.edit.content');
        $form->addTextArea('affiliate_script', 'form.advertisement.edit.affiliate-script');
        $form->addTextArea('affiliate_content', 'form.advertisement.edit.affiliate-content');

        // BUTTON
        $form->addSubmit('send', 'form.advertisement.edit.send');
        $form->addSubmit('send_back', 'form.advertisement.edit.send-back');
        $form->addSubmit('back', 'form.advertisement.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->advertisement->isLoaded()) {
            return [];
        }

        return [
            'name'              => $this->advertisement->getName(),
            'content'           => $this->advertisement->getContent(),
            'is_active'         => $this->advertisement->isActive(),
            'website'           => $this->advertisement->getWebsite(),
            'affiliate_script'  => $this->advertisement->getAffiliateScript(),
            'affiliate_content' => $this->advertisement->getAffiliateContent(),
        ];
    }
}
