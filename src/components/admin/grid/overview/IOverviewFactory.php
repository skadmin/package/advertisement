<?php

declare(strict_types=1);

namespace Skadmin\Advertisement\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
