<?php

declare(strict_types=1);

namespace Skadmin\Advertisement\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Advertisement\BaseControl;
use Skadmin\Advertisement\Doctrine\Advertisement\Advertisement;
use Skadmin\Advertisement\Doctrine\Advertisement\AdvertisementFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

class Overview extends GridControl
{
    use APackageControl;

    private AdvertisementFacade $facade;

    public function __construct(AdvertisementFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'advertisement.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.advertisement.overview.name')
            ->setRenderer(function (Advertisement $advertisement): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $advertisement->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($advertisement->getName());

                return $name;
            });
        $grid->addColumnText('website', 'grid.advertisement.overview.website')
            ->setRenderer(static function (Advertisement $advertisement): ?Html {
                if ($advertisement->getWebsite() === '') {
                    return null;
                }

                $icon = Html::el('small', ['class' => 'fas fa-external-link-alt mr-1']);

                return Html::el('a', [
                    'href'   => $advertisement->getWebsite(),
                    'target' => '_blank',
                ])->setHtml($icon)
                    ->addText($advertisement->getWebsite());
            });
        $grid->addColumnText('isActive', 'grid.advertisement.overview.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);

        // FILTER
        $grid->addFilterText('name', 'grid.advertisement.overview.name');
        $grid->addFilterText('website', 'grid.advertisement.overview.website');
        $grid->addFilterSelect('isActive', 'grid.advertisement.overview.is-active', Constant::PROMTP_ARR + Constant::DIAL_YES_NO)
            ->setTranslateOptions();

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.advertisement.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.advertisement.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['name' => 'ASC']);

        return $grid;
    }
}
