<?php

declare(strict_types=1);

namespace Skadmin\Advertisement;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'advertisement';
    public const DIR_IMAGE = 'advertisement';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-ad']),
            'items'   => ['overview'],
        ]);
    }
}
