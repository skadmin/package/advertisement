<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313090938 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'advertisement.overview', 'hash' => '5cb2201a367f89ff22570ce9e29bfa37', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Reklamy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'advertisement.overview.title', 'hash' => '75fe8a055d39404ab816ef2de95abce3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Reklamy|přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement.overview.action.new', 'hash' => '40aa0f3b2abf63d061ba3c740543a4ff', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit reklamu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement.overview.name', 'hash' => '2db8515085762751df5e368da0732f82', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement.overview.website', 'hash' => 'eaee202410d19d46d08c7994ecda0be1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement.overview.is-active', 'hash' => '8655556abd8d4cc966e581664f87a0a0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'advertisement.edit.title', 'hash' => '443edb3723981749746edd0547e8663b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení reklamy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.name', 'hash' => 'f30c325bc33aac320e68b24de8101f13', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.name.req', 'hash' => 'a30e7dea6f15887d2c1810765b47f990', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.website', 'hash' => '4d4d269492c962ac3167ed387f1b52b9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.image-preview', 'hash' => '65aa542825149caf3975e90938d39f8d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled reklamy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.image-preview.rule-image', 'hash' => '5bfcde679797d2de4828de231dfdfd90', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.content', 'hash' => '90a0ae011a2f17125a1b1b9efb055a6e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.affiliate-script', 'hash' => 'dd0f0097c17ef2c9f7bedeb56eb0aa73', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Affiliate head', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.affiliate-content', 'hash' => '2c7a998dfd96e310521b4338013a7c2a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Affiliate body', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.is-active', 'hash' => 'bfceff2c5bcef9d02c20725b12395805', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.send', 'hash' => 'b1ed8b28197372e05b016f6427d87b78', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.send-back', 'hash' => 'ff1d7e2b2faaabe9babde97d1e102a3d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.back', 'hash' => '07f5d4cce467f4b6d8c739be41ad4921', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.flash.success.create', 'hash' => '9009582ebaba031f422adbd56653b408', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Reklama byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.advertisement.overview.action.edit', 'hash' => '134182c496853625f7de9dc093ee353d', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'advertisement.edit.title - %s', 'hash' => '670856dcd7a40fa0ec57738e1e36457b', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace reklamy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.advertisement.edit.flash.success.update', 'hash' => 'bb93e0c8b40e53e68edaea712da011f3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Reklama byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.advertisement.title', 'hash' => '61506772913cc4b26be0176336d531c6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Reklamy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.advertisement.description', 'hash' => '3e8585b4f965a209d38b5e0e389ba671', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat reklamy', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
